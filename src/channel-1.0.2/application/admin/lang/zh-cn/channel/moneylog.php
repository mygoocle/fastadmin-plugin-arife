<?php

return [
    'Channel_id'       => '会员ID',
    'Money'            => '变更余额',
    'Before'           => '变更前余额',
    'After'            => '变更后余额',
    'Memo'             => '备注',
    'Createtime'       => '创建时间',
    'Channel.truename' => '真实姓名'
];
