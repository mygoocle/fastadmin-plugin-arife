<?php

return [
    'Id'               => 'ID',
    'Channel_id'       => '渠道ID',
    'Total_fee'        => '佣金',
    'Createtime'       => '创建时间',
    'Rel_table'        => '关联表',
    'Rel_id'           => '关联表主键',
    'Channel.truename' => '真实姓名'
];
