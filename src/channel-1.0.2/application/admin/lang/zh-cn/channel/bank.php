<?php

return [
    'Id'               => 'ID',
    'Channel_id'       => '关联渠道',
    'Type'             => '收款方式',
    'Type bank'        => '银行卡',
    'Type alipay'      => '支付宝',
    'Account'          => '账号',
    'Truename'         => '姓名',
    'Mobile'           => '手机号',
    'Bank_type'        => '开户行',
    'Admin_id'         => '账户ID',
    'Channel.truename' => '真实姓名',
    'Admin.avatar'     => '头像'
];
