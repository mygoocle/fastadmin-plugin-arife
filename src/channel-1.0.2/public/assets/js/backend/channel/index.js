define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'channel/index/index' + location.search,
                    add_url: 'channel/index/add',
                    edit_url: 'channel/index/edit',
                    del_url: 'channel/index/del',
                    multi_url: 'channel/index/multi',
                    import_url: 'channel/index/import',
                    table: 'channel',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'truename', title: __('Truename'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'money', title: __('余额(元)'), operate: 'LIKE'},
                        {field: 'idcard', title: __('Idcard'), operate: 'LIKE'},
                        {field: 'ratio', title: __('Ratio'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1'),"2":__('Status 2')}, formatter: Table.api.formatter.status},
                        // {field: 'code', title: '邀请码', operate: 'LIKE'},
                        {field: 'buttons',title:__('功能'),table:table,events: Table.api.events.operate,
                            buttons:[
                                {
                                    name: 'superlogin',
                                    text: __('超管一键登录'),
                                    title: __('超管一键登录'),
                                    classname: 'btn btn-xs btn-primary btn-ajax',
                                    url: 'channel/index/getSloginUrl',
                                    icon: 'fa fa-copy',
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        if(row.code){
                                            return true;
                                        }else{
                                            return false;
                                        }
                                    },
                                    success: function(data,ret){
                                        if(ret.code == 1){
                                            var url = ret.data.url;
                                            $("body").after("<input id='copyVal'></input>");
                                            var text = url;
                                            var input = document.getElementById("copyVal");
                                            input.value = text;
                                            input.select();
                                            input.setSelectionRange(0, input.value.length);
                                            document.execCommand("copy");
                                            $("#copyVal").remove();
                                            layer.msg("复制成功");
                                        }else{
                                            layer.msg(ret.msg);
                                        }
                                        return false;
                                    }
                                },
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});