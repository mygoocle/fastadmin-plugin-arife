define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'channel/bank/index' + location.search,
                    add_url: 'channel/bank/add',
                    edit_url: 'channel/bank/edit',
                    del_url: 'channel/bank/del',
                    multi_url: 'channel/bank/multi',
                    import_url: 'channel/bank/import',
                    table: 'channel_bank',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'type', title: __('Type'), searchList: {"bank":__('Type bank'),"alipay":__('Type alipay')}, formatter: Table.api.formatter.normal},
                        {field: 'account', title: __('收款账号'), operate: 'LIKE'},
                        {field: 'truename', title: __('收款人'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'bank_name', title: __('Bank_type'), operate: 'LIKE'},
                        {field: 'channel.truename', title: __('渠道名称'), operate: 'LIKE'},
                        // {field: 'channel.truename', title: __('Channel.truename'), operate: 'LIKE'},
                        // {field: 'admin.avatar', title: __('Admin.avatar'), operate: 'LIKE', events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});